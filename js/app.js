// Initial setup
var page = 1;
var itemsPerPage = 20;
var pagDiv = document.getElementById('pagination');

// Loading data from Github using AJAX
var request = new XMLHttpRequest();
request.onreadystatechange = function () {
  if (request.readyState === 4) {
    if (request.status === 200) {
      populateTable(page);
    } else {
      alert('An error occured while loading data from the Github API');
    }
  }
};
request.open('GET', 'https://api.github.com/search/repositories?q=language:javascript&sort=stars&order=desc&per_page=100');
request.send();

// Populating table with paginated data from the API call
function populateTable(page) {
  var repos = JSON.parse(request.response).items;
  var itemsTotal = repos.length;
  var firstItemOnPage = (itemsPerPage * page) - itemsPerPage;
  var lastItemOnPage = itemsPerPage * page;
  if (lastItemOnPage > itemsTotal) {
    lastItemOnPage = itemsTotal;
  }
  var numPages = repos.length / itemsPerPage;
  var rowsHtml = '';
  for (var i = firstItemOnPage; i < lastItemOnPage; ++i) {
    rowsHtml += '<tr>';
    rowsHtml += '<td>' + (i+1) + '</td>';
    rowsHtml += '<td><a href=' + repos[i].html_url + ' target=_BLANK>' + repos[i].name + '</a></td>';
    rowsHtml += '<td>' + repos[i].description + '</td>';
    rowsHtml += '<td><a href=' + repos[i].owner.html_url + ' target=_BLANK>' + repos[i].owner.login  + '</a></td>';
    rowsHtml += '<td>' + repos[i].watchers_count + '</td>';
    rowsHtml += '</tr>';
  }
  document.getElementById('table-body').innerHTML = rowsHtml;
  renderPagination(page, numPages);
  itemsInterval(firstItemOnPage, lastItemOnPage, itemsTotal);
  scroll(0,0);
}

// Render pagination buttons with appropriate CSS classes based on current page 
function renderPagination(page, numPages) {
  // Previous button
  pagDiv.innerHTML = '';
  var prevButton = document.createElement('SPAN');
  prevButton.setAttribute("class", "pag-button");
  prevButton.textContent = "Forrige";
  pagDiv.appendChild(prevButton);
  if (page === 1) {
    prevButton.classList.add('disabled');
  } else {
    prevButton.addEventListener('click', () => {
      var prevPage = page - 1;
      populateTable(prevPage);
    });
  }
  
  // A numbered button for each page
  for (var i=0; i < numPages; ++i) {
    var numButton = document.createElement('SPAN');
    numButton.setAttribute("class", "pag-button");
    numButton.textContent = i+1;
    pagDiv.appendChild(numButton);
    if ((i+1) === page) {
      numButton.classList.add("active");
    } else {
      (function(i) {
        numButton.addEventListener('click', () => {
          var newPage = i+1
          populateTable(newPage);
        });
      }(i));
    }
  }
  
  // Next button
  var nextButton = document.createElement('SPAN');
  nextButton.setAttribute("class", "pag-button");
  nextButton.textContent = "Neste";
  pagDiv.appendChild(nextButton);
  if (page === numPages) {
    nextButton.classList.add('disabled');
  } else {
    nextButton.addEventListener('click', () => {
      var nextPage = page + 1;
      populateTable(nextPage);
    });
  }
}

// Display the interval of items shown in the table
function itemsInterval (firstItem, lastItem, itemsTotal) {
  var topDiv = document.getElementById('top-interval');
  var bottomDiv = document.getElementById('bottom-interval');
  var interval = 'Viser nr. ' + (firstItem + 1) + ' til ' + lastItem + ' av totalt ' + itemsTotal;
  topDiv.textContent = interval;
  bottomDiv.textContent = interval;
}